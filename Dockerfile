FROM python

RUN mkdir -p /home/app
WORKDIR /home/app

COPY app.py .

RUN ["python", "app.py"]
